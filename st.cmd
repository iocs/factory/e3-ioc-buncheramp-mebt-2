require essioc
require iocmetadata
require mebtbuncheramp

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "1000000")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("PREFIX", "MEBT-010:RFS-PAmp-210")
epicsEnvSet("HOSTNAME", "mebt-sspa-02.tn.esss.lu.se")

drvAsynIPPortConfigure("SSPAConnection", $(HOSTNAME):$(MODBUS_PORT=502),0,0,1)
modbusInterposeConfig("SSPAConnection",0,1000,0)

## Registers

########## FC2 -> Read Discrete Input ##########
drvModbusAsynConfigure("FC2A", "SSPAConnection", 0,  2, 0x0800,  19,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2B", "SSPAConnection", 0,  2, 0x0880,   3,  0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC2C1", "SSPAConnection", 0,  2, 0x0813,   1,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2C2", "SSPAConnection", 0,  2, 0x0814,   1,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2C3", "SSPAConnection", 0,  2, 0x0815,   1,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2C4", "SSPAConnection", 0,  2, 0x0816,   1,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2C5", "SSPAConnection", 0,  2, 0x0817,   1,  0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC2D1", "SSPAConnection", 0,  2, 0x0890,  10,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2D2", "SSPAConnection", 0,  2, 0x089A,  10,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2D3", "SSPAConnection", 0,  2, 0x08A4,  10,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2D4", "SSPAConnection", 0,  2, 0x08AE,  10,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2D5", "SSPAConnection", 0,  2, 0x08B8,  10,  0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC2E", "SSPAConnection", 0,  2, 0x0818,   4,  0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC2F", "SSPAConnection", 0,  2, 0x081C,  4,  0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC2G", "SSPAConnection", 0,  2, 0x08C2,  8,  0, 500, "solidStatPwAmp")


########## FC3 -> Read Holding Register ##########
drvModbusAsynConfigure("FC3A", "SSPAConnection", 0,  3, 0x0000,   1, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC3B", "SSPAConnection", 0,  3, 0x0002,  20, 0, 500, "solidStatPwAmp")


########## FC4 -> INPUT REGISTERS
drvModbusAsynConfigure("FC4A", "SSPAConnection", 0,  4, 0x0080,   1, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4B", "SSPAConnection", 0,  4, 0x0100,  20, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4C", "SSPAConnection", 0,  4, 0x0093,   1, 0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC4D1", "SSPAConnection", 0,  4, 0x0094,   1, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4D2", "SSPAConnection", 0,  4, 0x0095,   1, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4D3", "SSPAConnection", 0,  4, 0x0096,   1, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4D4", "SSPAConnection", 0,  4, 0x0097,   1, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4D5", "SSPAConnection", 0,  4, 0x0098,   1, 0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC4E1", "SSPAConnection", 0,  4, 0x0114,  4, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4E2", "SSPAConnection", 0,  4, 0x0118,  4, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4E3", "SSPAConnection", 0,  4, 0x011C,  4, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4E4", "SSPAConnection", 0,  4, 0x0120,  4, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4E5", "SSPAConnection", 0,  4, 0x0124,  4, 0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC4F1", "SSPAConnection", 0,  4, 0x0200, 36, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4F2", "SSPAConnection", 0,  4, 0x0224, 36, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4F3", "SSPAConnection", 0,  4, 0x0248, 36, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4F4", "SSPAConnection", 0,  4, 0x026C, 36, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4F5", "SSPAConnection", 0,  4, 0x0290, 36, 0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC4G",  "SSPAConnection", 0,  4, 0x0099,  1, 0, 500, "solidStatPwAmp")

drvModbusAsynConfigure("FC4H",  "SSPAConnection", 0,  4, 0x02B4,  2, 0, 500, "solidStatPwAmp")

########## FC6 -> Write Single Holding Register ##########
drvModbusAsynConfigure("FC6A", "SSPAConnection", 0,  6, 0x0000,   1, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC6B", "SSPAConnection", 0,  6, 0x0001,   1, 0, 500, "solidStatPwAmp")

########## FC16 -> Write Multiple Holding Register ##########
drvModbusAsynConfigure("FC16", "SSPAConnection", 0,  16, 0x0002, 20, 0, 500, "solidStatPwAmp")

dbLoadRecords(RF_SSPA.template, "P=$(PREFIX):, R=")
dbLoadRecords(RF_SSPA_IM.template, "P=$(PREFIX):, R=IM1-, N_MODULE=1")
dbLoadRecords(RF_SSPA_IM.template, "P=$(PREFIX):, R=IM2-, N_MODULE=2")
dbLoadRecords(RF_SSPA_IM.template, "P=$(PREFIX):, R=IM3-, N_MODULE=3")
dbLoadRecords(RF_SSPA_IM.template, "P=$(PREFIX):, R=IM4-, N_MODULE=4")
dbLoadRecords(RF_SSPA_IM.template, "P=$(PREFIX):, R=IM5-, N_MODULE=5")

###############################################################################
# Combined state machine
###############################################################################
epicsEnvSet(FIM_PREFIX, "MEBT-010:RFS-FIM-201")
epicsEnvSet(FIM_FSM_SP, "$(FIM_PREFIX):FSM")
epicsEnvSet(FIM_FSM_RB, "$(FIM_PREFIX):FSM-RB")
epicsEnvSet(SSPA_FSM_SP, "$(PREFIX):FSM-Cmd")
epicsEnvSet(SSPA_FSM_RB, "$(PREFIX):FSM-RB")

#- load combined state machine
dbLoadRecords("$(mebtbuncheramp_DB)/RF_SSPA_FSM.template", "P=$(PREFIX):, R=, FIM_FSM_SP=$(FIM_FSM_SP), FIM_FSM_RB=$(FIM_FSM_RB), SSPA_FSM_SP=$(SSPA_FSM_SP), SSPA_FSM_RB=$(SSPA_FSM_RB)")

pvlistFromInfo("SAVRES_THIS", "$(IOCNAME):SavResList")

iocInit()

